# Oxford MM templates

Online viewer: https://open.win.ox.ac.uk/pages/fsl/oxford-mm-templates/

Git LFS is required when cloning this repository. Alternatively, it can be downloaded as a compressed folder (next to the 'Clone' button) without having to install Git LFS.
All templates, the OMM-1 and the age-dependent templates associated with OMM-1, can also be downloaded from OSF with the following link/DOI: https://doi.org/10.17605/OSF.IO/S9GE4

# Oxford-MM-1 (OMM-1)

![Oxford-MM-1 T1, T2 FLAIR, and DTI volumes](/Oxford-MM-1/oxford-mm-1_overview.jpg "Oxford-MM-1 T1, T2 FLAIR, and DTI volumes")*Oxford-MM-1 T1, T2 FLAIR, and DTI volumes*


![Oxford-MM-1 T2 FLAIR overlayed with DTI volume](/Oxford-MM-1/oxford-mm-1_T2+tensor.png "Oxford-MM-1 T2 FLAIR overlayed with DTI volume")*Oxford-MM-1 T2 FLAIR overlayed with DTI volume*


Version 1 of our multimodal asymmetric template with its T1, T2 FLAIR and DTI volumes.
It was constructed from **240 UK Biobank individuals uniformly sampled from the age range 50-55 years (50% female/male)**.
We believe a template from this age range will be useful for a wealth of studies.
Note that the template was **rigidly** transformed to MNI space to capture the population’s average brain shape and size, and avoid the introduction of unrepresentative scaling effects. 

We used the pipeline described in:<br>
Arthofer, C., Smith, S.M., Jenkinson, M., Andersson, J., Lange, F. Multimodal MRI template construction from UK Biobank: Oxford-MM-0. (2021). Organisation for Human Brain Mapping (OHBM)

Several improvements to the pipeline include the use of:<br>
- weighted brain masks in T1 and DTI reference space, as well as in subjects' native DTI spaces to avoid the introduction of noise around the brain
- FSL's epi_reg with BBR for more accurate rigid registrations between dMRI and T1
- median (instead of mean) template for the first few iterations to provide a better initialization
- non-defaced images

Our general pipeline for template construction can be downloaded from https://git.fmrib.ox.ac.uk/cart/mm-template-construction.

| File | Description |
|---|---|
| `OMM-1_T1_head.nii.gz` | T1 whole-head volume
| `OMM-1_T1_brain.nii.gz` | Brain-masked OMM-1_T1_head
| `OMM-1_T2_FLAIR_head.nii.gz` | T2 FLAIR whole-head volume
| `OMM-1_T2_FLAIR_brain.nii.gz` | Brain-masked OMM-1_T2_FLAIR_head
| `OMM-1_DTI_tensor.nii.gz` | 4D tensor volume
| `OMM-1_DTI_FA.nii.gz` | Fractional anisotropy
| `OMM-1_DTI_MD.nii.gz` | Mean diffusivity
| `OMM-1_DTI_MO.nii.gz` | Mode of the anisotropy
| `OMM-1_DTI_L1.nii.gz` | 1st eigenvalue
| `OMM-1_DTI_L2.nii.gz` | 2nd eigenvalue
| `OMM-1_DTI_L3.nii.gz` | 3rd eigenvalue
| `OMM-1_DTI_V1.nii.gz` | 1st eigenvector
| `OMM-1_DTI_V2.nii.gz` | 2nd eigenvector
| `OMM-1_DTI_V3.nii.gz` | 3rd eigenvector
| `OMM-1_DTI_FA_skeleton.nii.gz` | Skeletonised fractional anisotropy
| `OMM-1_QSM.nii.gz` | Quantitative Susceptibility Map
| `OMM-1_T1_brain_mask_average.nii.gz` | Average brain mask
| `OMM-1_T1_brain_mask_weighted.nii.gz` | Weighted brain mask for registrations with whole-head images only; reduces regularisation in the brain compared to skull and surrounding regions (can be used as --mask_ref_scalar input to MMORF)
| `OMM-1_DTI_mask_average.nii.gz` | Average brain mask for the DTI volume
| `OMM-1_DTI_mask_weighted.nii.gz` | Weighted brain mask for the DTI volume to reduce the impact of noise around the edges of the brain during registrations (can be used as --mask_ref_tensor input to MMORF)
| `transformations/MNI152NLIN6Asym_to_OMM-1_warp.nii.gz` | Combined warp (affine + nonlinear) to transform images (e.g., masks) from the space of FSL’s MNI 152 template to the space of the OMM-1 template. Directly compatible with FSL’s applywarp and vecreg function.
| `transformations/OMM-1_to_MNI152NLIN6Asym_warp.nii.gz` | Combined warp (affine + nonlinear) to transform images (e.g., masks) from the space of the OMM-1 template to the space of FSL’s MNI 152 template. Directly compatible with FSL’s applywarp and vecreg function.

# Oxford-MM-0 (OMM-0)

![Oxford-MM-0 T1, T2 FLAIR, and DTI volumes](/Oxford-MM-0/oxford-mm-0_overview.png "Oxford-MM-0 T1, T2 FLAIR, and DTI volumes")*Oxford-MM-0 T1, T2 FLAIR, and DTI volumes*

Version 0 of our multimodal template with its T1, T2 FLAIR and DTI volumes. It was constructed from **713 UK Biobank individuals (uniformly sampled from the age range 45-81 years, 50% female/male)**.
Note that the template was **rigidly** transformed to MNI space to capture the population’s average brain shape and size, and avoid the introduction of unrepresentative scaling effects.

More information about the pipeline and how to cite:<br>
Arthofer, C., Smith, S.M., Jenkinson, M., Andersson, J., Lange, F. Multimodal MRI template construction from UK Biobank: Oxford-MM-0. (2021). Organisation for Human Brain Mapping (OHBM)

Our general pipeline for template construction can be downloaded from https://git.fmrib.ox.ac.uk/cart/mm-template-construction.

| File | Description |
|---|---|
| `OMM-0_T1_head.nii.gz` | T1 whole-head volume
| `OMM-0_T1_brain.nii.gz` | Brain-masked OMM-0_T1_head
| `OMM-0_T1_head_clamped.nii.gz` | T1 whole-head volume with clamped high intensities in the skull
| `OMM-0_T1_brain_clamped.nii.gz` | Brain-masked oxford-mm-0_T1_head_clamped
| `OMM-0_T2_FLAIR_head.nii.gz` | T2 FLAIR whole-head volume
| `OMM-0_T2_FLAIR_brain.nii.gz` | Brain-masked OMM-0_T2_FLAIR_head
| `OMM-0_DTI_tensor.nii.gz` | 4D tensor volume
| `OMM-0_DTI_FA.nii.gz` | Fractional anisotropy
| `OMM-0_DTI_MD.nii.gz` | Mean diffusivity
| `OMM-0_DTI_MO.nii.gz` | Mode of the anisotropy
| `OMM-0_DTI_L1.nii.gz` | 1st eigenvalue
| `OMM-0_DTI_L2.nii.gz` | 2nd eigenvalue
| `OMM-0_DTI_L3.nii.gz` | 3rd eigenvalue
| `OMM-0_DTI_V1.nii.gz` | 1st eigenvector
| `OMM-0_DTI_V2.nii.gz` | 2nd eigenvector
| `OMM-0_DTI_V3.nii.gz` | 3rd eigenvector
| `OMM-0_T1_brain_mask_average.nii.gz` | Average brain mask
| `OMM-0_T1_brain_mask_weighted.nii.gz` | Weighted brain mask for registrations with whole-head images only; reduces regularisation in the brain compared to skull and surrounding regions (can be used as --mask_ref_scalar input to MMORF)
| `OMM-0_DTI_mask.nii.gz` | Weighted brain mask for the DTI volume to reduce the impact of noise around the edges of the brain (can be used as --mask_ref_tensor input to MMORF)

Contact: christoph.arthofer@ndcn.ox.ac.uk

DOI 10.17605/OSF.IO/S9GE4

Copyright, 2023, University of Oxford. All rights reserved
